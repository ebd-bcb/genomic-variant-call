# Workflow to execute a genomic variant call running a docker image in Kubernetes.
---

## build a docker image by reading the dockerfile:
> we will use the Dockerfile to build an Ubuntu image containig all the software needed to run the pipeline:

``docker build --no-cache -t ebdbcb/genomic_variant_call .``

> **Note**: the folder `pkg/` is required to be placed in the same location as the dockerfile to build the image.

## list docker images:
``docker images``

---
## test the docker image locally from your computer:
### execute the docker image and mount a volume with your scripts and data from a terminal:
``docker run -it -v /path_to_your_data:/data ebdbcb/genomic_variant_call /bin/bash``

> assuming your script and data are stored locally in a folder named *data*, you will see its content in the container, in the mount point named `data/`

### execute the bash script:
#### first, make it executable:
``chmod 770 run.sh``

#### then, run it:
``./run.sh``

### execute the following command from another terminal to access the same container:

#### first, find out the container ID:
``docker ps``

#### then, access the same container:
``docker exec -it containerID /bin/bash``

#### find out how much memory is used:
``echo $(ps aux | grep /script | awk '{ print $1 }' | head -n 1) | xargs pmap | tail -n 1``
    
> ... we use this information to get a more precise estimate of the RAM required, so that we can specify it in the *genomic_variant_call.yaml* file.

#### exit from the docker image in both terminals:

``exit``

---

## once we have checked that everything works as it should ...
### upload the following data to our remote repository in gitlab

> **Note**: data folders listed here are empty, they are named just to detail the structure of the directory.

- ``Dockerfile``
    > to allow anyone build the same docker image
- ``pkg/``
    > folder required to build the same docker image
- ``data/ref_genome/``
    > folder containing the genome reference sequence (*.fa* file), its index (*fa.fai* file) and the sequence dictionary (*.dict* file)
- ``data/bams/``
    > folder containing the *bam* file and its index (*bam.bai*)
- ``data/beds/``
    > folder containing the *bed* file that specifies the genomic region of interest in the genome where variants are to be called
- ``data/input_file.txt``
    > a text file containing the parameters used by the bash script to execute the genomic variant call
- ``data/run.sh``
    > the bash script that will perform the genomic variant call
- ``genomic_variant_call.yaml``
    > yaml file to be run in kubernetes
- ``readme.md``
    > brief explanation of the genomic variant call to be performed
- ``how_to.md``
    > current file

### upload the image to DockerHub:
``docker push ebdbcb/genomic_variant_call``

### finally, from our computer cluster, run the job:
kubectl apply -f genomic_variant_call.yaml

---