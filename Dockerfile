# base image:
FROM ubuntu:22.04 as genomic_variant_call
ARG DEBIAN_FRONTEND=noninteractive

# install packages:
RUN apt-get update && apt-get install -y \
    wget \
    openjdk-11-jdk \
    ca-certificates \
    unzip \
    bzip2

# install dependencies:
WORKDIR /dependencies

## gatk
### install conda:
COPY pkg/Miniconda3-4.3.11-Linux-x86_64.sh .
RUN chmod +x Miniconda3-4.3.11-Linux-x86_64.sh && \
    ./Miniconda3-4.3.11-Linux-x86_64.sh -b -p /opt/conda
   
### add conda to path:
ENV PATH=/opt/conda/bin:$PATH

### install GATK 4.2:
COPY pkg/gatk-4.2.6.1.zip . 
RUN unzip gatk-4.2.6.1.zip -d /opt/
RUN ln -s /opt/gatk-4.2.6.1/gatk /usr/local/bin/gatk

### create symlink to java:
RUN ln -s /usr/bin/java /opt/conda/bin/java

# clean packages:
WORKDIR /
RUN apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# clean dependencies:
RUN rm -R /dependencies/*
