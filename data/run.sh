#!/bin/bash

# print start time
start_time=$(date +"%Y-%m-%d %H:%M:%S")
echo "Start Time: $start_time"

# the script takes as first positional argument an input text file:
input=${1}

# and as second positional argument a bed file of the regions to be called
bed=${2}

# function to parse the input text file
parse_yaml() {
    local input_file="$1"
    local grep_word="$2"

    if [[ ! -f "$input_file" ]]; then
        echo "Error: input file '$input_file' not found."
        return 1
    fi

    local grep_result=$(grep "$grep_word" "$input_file")

    if [[ -z "$grep_result" ]]; then
        echo "Error: Word '$grep_word' not found in YAML file."
        return 1
    fi

    local trimmed_result=$(echo "$grep_result" | cut -d':' -f2 | sed -e 's/^[[:space:]]*//' | sed -e "s/^'//" -e "s/'$//")

    echo "$trimmed_result"
}

# read variables from input text file
reference_genome=$(parse_yaml $input "reference_genome")
input_bam=$(parse_yaml $input "input_bam")
output_gvcf=$(parse_yaml $input "output_gvcf")

gatk HaplotypeCaller  \
   -R $reference_genome \
   -I $input_bam \
   -O $output_gvcf \
   -L $bed \
   --native-pair-hmm-threads 4 \
   -ERC GVCF

# print finish time
finish_time=$(date +"%Y-%m-%d %H:%M:%S")
echo "Finish Time: $finish_time"
