## Structure of the data folder.

The following files have been provided by [Enrico Bazzicalupo]( https://www.researchgate.net/profile/Enrico-Bazzicalupo):

- input_file.txt
- run.sh

The folders are empty but they should contain, in your local machine, the data files described in the [workflow](../how_to.md). 

## Run the bash script:

`chmod 755 run.sh`

`./run.sh input_file.txt beds/file.bed`
